

export interface Resident {
    DOB: Date;
    firstName: string;
    lastName: string;
    mobileNo: string;
    secondaryMobile: string;
}

