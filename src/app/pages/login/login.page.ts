import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Resident } from 'src/app/models/owner.model';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  mobileNumber: any;
  otpTriggered: any = true;
  code: '';
  verificationId: any;
  residentsList: any;
  residents$: Observable<any>;
  residentCol: AngularFirestoreCollection<Resident>;

  constructor(
    public afAuth: AngularFireAuth,
    private afs: AngularFirestore,
    public router: Router,
    public loadingCtrl: LoadingController,
    public toastCtrl : ToastController,
    private storage: Storage,

  ) { }

  ngOnInit() {
  }


  async onOtpSend() {
    if (this.mobileNumber) {
      // const loading = await this.loadingCtrl.create({
      //   message: 'Generating OTP...',
      //   translucent: true,
      // });
      // await loading.present();
      console.log('if works');
       
      this.residentCol = this.afs.collection('flatOwners', ref => {
        console.log('ref works');
        return ref.where('mobileNo', '==', '+' + 91 + this.mobileNumber);
      });

      this.residents$ = this.residentCol.snapshotChanges().pipe(map(actions => {
        return actions.map(a => {
          console.log('action works');
          const data = a.payload.doc.data();
          console.log(data);
          return { data };
        });
      }));

      this.residents$.subscribe(snapshot => {
        if (snapshot.length === 0) {
          this.checksecondnumber();
        } else {
          // loading.dismiss();
          this.sendotp();
          this.otpTriggered = true;
        } 
      });

    } else { 
      alert('Add Mobile Number')
    }
  }


  checksecondnumber() {
    this.residentCol = this.afs.collection('flatOwners', ref => {
      return ref.where('secondaryMobile', '==', '+' + 91 + this.mobileNumber);
    });

    this.residents$ = this.residentCol.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data();
        return { data };
      });
    }));

    this.residents$.subscribe(snapshot => {
      if (snapshot.length === 0) {
        // alert('Sorry! Your number is not registered.')
        this.presentToast('Sorry! Your number is not registered');
      } else {
        this.sendotp();
        this.otpTriggered = false;
      }
    });
  }

  async verify() {
    // const loading = await this.loadingCtrl.create({
    //   message: 'Logging in...',
    //   translucent: true,
    // });
    // await loading.present();
    const signInCredential = firebase.auth.PhoneAuthProvider.credential(this.verificationId, '' + this.code);
    try {
      firebase.auth().signInWithCredential(signInCredential).then((success) => {
        console.log(success);
        // loading.dismiss();
        // this.storage.set('resident_info',success).then(()=>{
        // })
        this.router.navigate(['dashboard']);
      }).catch((err) => {
        console.log(err);
      });
    } catch (e) {
      console.log(e); 
    }
  }

  sendotp() {
    this.afAuth
      .signInWithPhoneNumber('+' + 91 + this.mobileNumber, new firebase.auth.RecaptchaVerifier
        (
          're-container', {
          size: 'invisible'
        }))
      .then((creds) => {
        alert(creds);
        console.log('creds', creds);
        this.verificationId = creds.verificationId;
        console.log(this.otpTriggered);
        this.otpTriggered = false;
      })
      .catch(err => {
        console.log('error', err);
      });
  }

  async presentToast(msg) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
