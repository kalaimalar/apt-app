import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { MenuController, AnimationController } from '@ionic/angular';
import { createAnimation, Animation } from '@ionic/core';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],

})
export class DashboardPage implements OnInit {

  @ViewChild('rectangle') rectangle: ElementRef;
  slideDiv: any;
  expanded: any = false;
  openUp: any = false;
  constructor(
    public menuCtrl: MenuController,
    public animationCtrl: AnimationController
  ) { }

  
  ngOnInit() {

  }



  showMenu() {
    this.menuCtrl.toggle();
  }

  iconClick() {
    this.openUp = !this.openUp;
  }
}
